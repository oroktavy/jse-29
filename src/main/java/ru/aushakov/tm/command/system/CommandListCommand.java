package ru.aushakov.tm.command.system;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.command.AbstractCommand;
import ru.aushakov.tm.constant.TerminalConst;

import java.util.Collection;

public final class CommandListCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.CMD_COMMANDS;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show application commands";
    }

    @Override
    public void execute() {
        System.out.println("[COMMAND LIST]");
        @NotNull final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getTerminalCommands();
        for (@NotNull final AbstractCommand command : commands) {
            @Nullable final String name = command.getName();
            if (StringUtils.isEmpty(name)) continue;
            System.out.println(name);
        }
    }

}
