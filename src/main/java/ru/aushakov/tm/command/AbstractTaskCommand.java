package ru.aushakov.tm.command;

import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.exception.entity.TaskNotFoundException;
import ru.aushakov.tm.model.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTask(@Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("STATUS: " + task.getStatus().getDisplayName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("CREATED: " + task.getCreated());
        System.out.println("START DATE: " + task.getStartDate());
        System.out.println("END DATE: " + task.getEndDate());
    }

}
