package ru.aushakov.tm.api.service;

import ru.aushakov.tm.model.Task;

import java.util.List;

public interface ITaskService extends IBusinessService<Task> {

    Task assignTaskToProject(String taskId, String projectId, String userId);

    Task unbindTaskFromProject(String taskId, String userId);

    List<Task> findAllTasksByProjectId(String projectId, String userId);

}
