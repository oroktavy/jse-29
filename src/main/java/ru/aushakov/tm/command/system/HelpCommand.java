package ru.aushakov.tm.command.system;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import ru.aushakov.tm.command.AbstractCommand;
import ru.aushakov.tm.constant.ArgumentConst;
import ru.aushakov.tm.constant.TerminalConst;

import java.util.Collection;

public final class HelpCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.CMD_HELP;
    }

    @Override
    @NotNull
    public String getArgument() {
        return ArgumentConst.ARG_HELP;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show possible options";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getTerminalCommands();
        for (@NotNull final AbstractCommand command : commands) {
            if (StringUtils.isEmpty(command.getName())) continue;
            System.out.println(command);
        }
    }

}
