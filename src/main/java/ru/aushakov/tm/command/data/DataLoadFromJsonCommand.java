package ru.aushakov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.api.service.IDataService;
import ru.aushakov.tm.command.AbstractDataCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.dto.Domain;
import ru.aushakov.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataLoadFromJsonCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.DATA_LOAD_FROM_JSON;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Load application data from json format";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD FROM JSON]");
        @NotNull final IDataService dataService = serviceLocator.getDataService();
        @NotNull final String fileJson = dataService.getFileJson();
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(fileJson)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        dataService.setDomain(domain);
        serviceLocator.getAuthService().logout();
    }

}
