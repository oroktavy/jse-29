package ru.aushakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.api.service.IDataService;
import ru.aushakov.tm.command.AbstractDataCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.dto.Domain;
import ru.aushakov.tm.enumerated.Role;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;

public final class DataSaveToBinaryCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.DATA_SAVE_TO_BINARY;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Save application data to binary format";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE TO BINARY]");
        @NotNull final IDataService dataService = serviceLocator.getDataService();
        @NotNull final Domain domain = dataService.getDomain();
        @NotNull final String fileBinary = dataService.getFileBinary();
        @NotNull final File file = new File(fileBinary);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.close();
    }

}
