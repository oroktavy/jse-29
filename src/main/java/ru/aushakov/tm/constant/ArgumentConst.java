package ru.aushakov.tm.constant;

import org.jetbrains.annotations.NotNull;

public interface ArgumentConst {

    @NotNull String ARG_VERSION = "-v";

    @NotNull String ARG_ABOUT = "-a";

    @NotNull String ARG_HELP = "-h";

    @NotNull String ARG_INFO = "-i";

}
