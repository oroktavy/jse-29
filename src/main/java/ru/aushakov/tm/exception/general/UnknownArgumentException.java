package ru.aushakov.tm.exception.general;

import org.jetbrains.annotations.Nullable;

public class UnknownArgumentException extends RuntimeException {

    public UnknownArgumentException(@Nullable final String arg) {
        super("Argument '" + arg + "' is not supported");
    }

}
