package ru.aushakov.tm.command.data;

import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.api.service.IDataService;
import ru.aushakov.tm.command.AbstractDataCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.dto.Domain;
import ru.aushakov.tm.enumerated.Role;

import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;

public final class DataSaveToXmlCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.DATA_SAVE_TO_XML;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Save application data to xml format";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE TO XML]");
        @NotNull final IDataService dataService = serviceLocator.getDataService();
        @NotNull final Domain domain = dataService.getDomain();
        @NotNull final String fileXml = dataService.getFileXml();
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final AnnotationIntrospector annotationIntrospector =
                new JaxbAnnotationIntrospector(objectMapper.getTypeFactory());
        objectMapper.setAnnotationIntrospector(annotationIntrospector);
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(fileXml);
        fileOutputStream.write(xml.getBytes(StandardCharsets.UTF_8));
        fileOutputStream.close();
    }

}
